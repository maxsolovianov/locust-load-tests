# start testing:
# locust --host=https://test-1.365pronto.net
# locust --host=https://test-2.365pronto.net ProntoWebUser CustomerWebUser

# stop testing:
# sudo kill `sudo lsof -t -i:8089`

from locust import Locust, HttpLocust, TaskSet, TaskSequence, task, seq_task
import json
import random
import uuid
import requests
from db import *

# for debugging
HOST = 'https://test-2.365pronto.net'
PASSWORD = 'Testtest1@'


def pronto_login(l, active_pronto_emails_list):
    print '### pronto_login: ###'
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    pronto_email = random.choice(active_pronto_emails_list)
    payload = {"email": pronto_email, "password": "Testtest1@"}
    resp = l.client.post('/api/users/login', data=json.dumps(payload),
                         headers=headers)
    assert resp.status_code == 200
    return resp.json()
    # print resp.content


def pronto_re_login(l, access, refresh):
    print '### pronto_re_login: ###'
    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access}
    payload = {"refreshToken": refresh}
    r = l.client.post(HOST + '/api/users/login/refresh',
                      headers=headers,
                      data=json.dumps(payload))
    assert r.status_code == 200


def pronto_logout(l, access):
    print "### pronto logout: ###"
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    r = l.client.post('/api/users/logout', headers=headers)
    assert r.status_code == 200
    print r.content


def create_pronto_user(l, access):
    print '### create pronto user: ###'
    random_uuid = str(uuid.uuid4())[:8]
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    payload = {"email": "365prontotest+" + random_uuid + "@gmail.com",
               "firstName": random_uuid,
               "lastName": random_uuid}
    resp = l.client.post('/api/pronto/users', data=json.dumps(payload), headers=headers)
    resp_dict = resp.json()
    # print payload
    # print resp.url
    assert resp.status_code == 200
    print resp_dict["email"]
    return resp_dict["email"]


def get_users_list(l, access):
    print '### get users list: ###'
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/users", params=payload, headers=headers)
    assert resp.status_code == 200
    # print resp.content


def get_customers_list(l, access):
    print '### get customers list: ###'
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/customers", params=payload, headers=headers)
    assert resp.status_code == 200
    # print resp.content


def create_office_contact(l, access, list_of_approved_providers):
    print '### create office contact: ###'
    random_uuid = str(uuid.uuid4())[:8]
    provider_id = random.choice(list_of_approved_providers)
    office_id = get_provider_main_office_id_by_provider_id_from_db(provider_id)
    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access}
    payload = {"contactType": "ALTERNATIVE", "email": "365prontotest+" + random_uuid + "@gmail.com",
               "firstName": random_uuid, "lastName": random_uuid, "officeId": office_id, "phoneNumber": 80983989487}
    resp = l.client.post("/api/providers/"
                         + str(provider_id) + "/offices/" +
                         str(office_id) + "/contacts",
                         headers=headers,
                         data=json.dumps(payload))
    assert resp.status_code == 200
    print 'provider id ' + str(provider_id)
    print 'office id ' + str(office_id)
    last_created_contact = get_last_created_contact_id_by_office_id(office_id)
    return {"provider_id": provider_id, "office_id": office_id, "created_contact": last_created_contact}


def update_office_contact(l, access, dict_with_provider_id_and_office_id):
    print '### update office contact: ###'
    random_uuid = str(uuid.uuid4())[:8]

    provider_id = dict_with_provider_id_and_office_id["provider_id"]
    office_id = dict_with_provider_id_and_office_id["office_id"]
    created_contact = dict_with_provider_id_and_office_id["created_contact"]

    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access}
    payload = {"contactType": "ALTERNATIVE", "email": "365prontotest+" + random_uuid + "@gmail.com",
               "firstName": random_uuid, "lastName": random_uuid, "officeId": office_id, "phoneNumber": 80983989487}
    resp = l.client.put("/api/providers/" +
                        str(provider_id) + "/offices/" +
                        str(office_id) + "/contacts/" +
                        str(created_contact),
                        headers=headers,
                        data=json.dumps(payload))
    print resp.url
    print resp.content
    assert resp.status_code == 200
    return resp.url


def delete_office_contact(l, access, url_to_delete):
    print '### delete office contact: ###'
    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access}
    resp = l.client.delete(url_to_delete.split("net", 1)[1], headers=headers)
    # print resp.content
    # print resp.url
    # print resp.status_code
    assert resp.status_code == 204


def approve_specified_customer_by_pronto(l, customer_name, access):
    print '### approve specified customer by pronto: ###'
    payload = {'reason': 'some reason', 'status': 'APPROVED'}
    customer_id = get_customer_id_by_customer_name(customer_name)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}

    resp = l.client.put("/api/customers/" + str(customer_id) + "/status",
                        data=json.dumps(payload), headers=headers)
    assert resp.status_code == 200
    # print resp.content


def approve_specified_provider_by_pronto(l, provider_name, access):
    print '### approve specified provider by pronto: ###'
    payload = {'reason': 'some reason', 'status': 'APPROVED'}
    customer_id = get_provider_id_by_provider_name(provider_name)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}

    resp = l.client.put("/api/providers/" + str(customer_id) + "/status",
                        data=json.dumps(payload), headers=headers, stream=True)

    if resp.status_code != 200:
        with open("Output.txt", "w") as text_file:
            text_file.write("approve_specified_provider_by_pronto: %s" % resp.content + resp.raw.data)

    assert resp.status_code == 200
    # print resp.content


def get_providers_list(l, access):
    print '### get providers list ###'
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/providers", params=payload, headers=headers)
    assert resp.status_code == 200
    # print resp.content


def get_provider_offices_list(l, access, list_approved_providers_ids):
    print '### get provider offices list: ###'
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(list_approved_providers_ids)
    resp = l.client.get("/api/providers/" + str(provider_id) + "/offices",
                        params=payload, headers=headers)
    assert resp.status_code == 200
    print resp.content


def get_states_list(l, access):
    print '### get states list: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/spatial/states",
                        headers=headers)
    assert resp.status_code == 200
    # print resp.content


def get_provider_overview(l, access, approved_providers_list):
    print '### get provider overview: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(approved_providers_list)
    resp = l.client.get("/api/providers/" + str(provider_id),
                        headers=headers)
    assert resp.status_code == 200
    # print resp.content


def customer_onboard(l):
    print '### customer onboarding: ###'
    random_uuid = str(uuid.uuid4())[:8]
    params = {'customerType': 'COMMERCIAL'}
    headers = {"Content-Type": "application/json"}
    body = '{"details":{"companyName":"' + random_uuid + '","contactFirstName":"' + random_uuid + '","contactLastName":"' + random_uuid + '","contactPhoneNumber":"+380983989487","contactEmail":"365prontotest+' + random_uuid + '@gmail.com","companyOverview":"","assetsOverview":""},"address":{"placeId":"ChIJHxOXx6pZwokRcr8qQ0-yGhE","streetAddress1":"1095 6th Avenue, New York, NY, USA","streetAddress2":"","city":"New York","state":"NY","zip":"10036"},"adminUser":{"firstName":"Test","lastName":"Test","email":"365prontotest+' + random_uuid + '@gmail.com"}}'
    resp = l.client.post("/api/customers", params=params, headers=headers, data=body)
    assert resp.status_code == 200
    # print resp.content
    return random_uuid


def finish_user_registration(l, registration_token):
    print '### finish registration: ###'
    headers = {"Content-Type": "application/json"}
    payload = {'acceptedTermsAndConditions': True, 'password': 'Testtest1@'}
    resp = l.client.post("/api/users/registration/" + registration_token, headers=headers,
                         data=json.dumps(payload))
    print resp.url
    assert resp.status_code == 200
    # print resp.content


def customer_login(l, list_of_approved_customers_emails):
    print '### customer_login: ###'
    customer_email = random.choice(list_of_approved_customers_emails)
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    payload = {"email": customer_email, "password": PASSWORD}
    resp = l.client.post('/api/users/login', data=json.dumps(payload),
                         headers=headers)
    # print 'login as user: ' + customer_email
    assert resp.status_code == 200
    # print resp.content
    resp_dict = resp.json()
    return resp_dict


def provider_login(l, list_of_approved_providers_emails):
    print '### provider_login: ###'
    provider_email = random.choice(list_of_approved_providers_emails)
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    payload = {"email": provider_email, "password": PASSWORD}
    resp = l.client.post('/api/users/login', data=json.dumps(payload),
                         headers=headers)
    print 'login as user: ' + provider_email
    assert resp.status_code == 200
    # print resp.content
    resp_dict = resp.json()
    return resp_dict


def user_re_login(l, access, refresh):
    print '### user_re_login: ###'
    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access}
    payload = {"refreshToken": refresh}
    r = l.client.post('/api/users/login/refresh',
                      headers=headers,
                      data=json.dumps(payload))
    assert r.status_code == 200
    # print r.content


def user_logout(l, access_token):
    print "### user logout: ###"
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access_token}
    r = l.client.post('/api/users/logout', headers=headers)
    assert r.status_code == 200
    # print r.content


def get_users_party_details(l, list_of_users_ids, access_token):
    print '### get users party details: ###'
    customer_id = random.choice(list_of_users_ids)
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access_token}
    resp = l.client.get("/api/core/users/" + str(customer_id) + "/party",
                        params=payload, headers=headers)
    # print "random id: " + str(customer_id)
    assert resp.status_code == 200
    # print resp.content


def get_customer_onboarding_info(l, list_of_custumers_ids, access):
    print '### get customer onboarding info by customer: ###'
    customer_id = random.choice(list_of_custumers_ids)
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/customers/" + str(customer_id) + "/onboarding",
                        params=payload, headers=headers)
    # print "random id: " + str(customer_id)
    assert resp.status_code == 200
    # print resp.content


def get_provider_onboarding_info(l, list_of_providers_ids, access):
    print '### get provider onboarding info by pronto: ###'
    customer_id = random.choice(list_of_providers_ids)
    payload = {'pageNumber': 0, 'pageSize': 200}
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.get("/api/providers/" + str(customer_id) + "/onboarding",
                        params=payload, headers=headers)
    # print "random id: " + str(customer_id)
    assert resp.status_code == 200
    # print resp.url
    # print resp.content


def create_customer():
    print '### create customer: ###'
    random_uuid = str(uuid.uuid4())[:8]
    params = {'customerType': 'COMMERCIAL'}
    headers = {"Content-Type": "application/json"}
    body = '{"details":{"companyName":"' + random_uuid + '","contactFirstName":"' + random_uuid + '","contactLastName":"' + random_uuid + '","contactPhoneNumber":"+380983989487","contactEmail":"365prontotest+' + random_uuid + '@gmail.com","companyOverview":"","assetsOverview":""},"address":{"placeId":"ChIJHxOXx6pZwokRcr8qQ0-yGhE","streetAddress1":"1095 6th Avenue, New York, NY, USA","streetAddress2":"","city":"New York","state":"NY","zip":"10036"},"adminUser":{"firstName":"Test","lastName":"Test","email":"365prontotest+' + random_uuid + '@gmail.com"}}'
    resp = requests.post(HOST + "/api/customers", params=params, headers=headers, data=body)
    assert resp.status_code == 200
    # print resp.content
    return random_uuid


def create_provider_office(l, access, list_of_approved_providers):
    print '### create provider office: ###'
    random_uuid = str(uuid.uuid4())[:8]
    provider_id = random.choice(list_of_approved_providers)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    body = '{"address":{"placeId":"ChIJvTTvux5awokRZ_sbLu1ASaI","streetAddress1":"270 Greenwich Street, New York, NY, USA","streetAddress2":"","city":"New York","state":"NY","zip":"10007"}, "officeName": "' + random_uuid + '", "officeType": "BRANCH", "serviceArea": 200}'
    # print body
    resp = l.client.post("/api/providers/" + str(provider_id) + "/offices", headers=headers, data=body)
    print resp.url
    # print resp.content
    assert resp.status_code == 200
    # print resp.content


def update_provider_overview(l, access, list_of_approved_providers):
    print '### update provider overview: ###'
    provider_id = random.choice(list_of_approved_providers)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    body = '{"numberOfEmployees":"1-10","numberOfServiceVehicles":"1-10","website":"","nabcepRelation":{"id":5006,"memberNabcep":false,"url":null,"validationStatus":"NOT_APPLICABLE"},"uapwRequirements":"NO"}'
    # print body
    resp = l.client.patch("/api/providers/" + str(provider_id), headers=headers, data=body)
    # print resp.url
    # print resp.content
    assert resp.status_code == 200


def get_provider_overview(l, access, approved_providers_list):
    print '### get provider overview: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(approved_providers_list)
    resp = l.client.get("/api/providers/" + str(provider_id),
                        headers=headers)
    assert resp.status_code == 200
    # print resp.content


def get_provider_contact_list(l, access, approved_providers_list):
    print '### get provider contact list: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(approved_providers_list)
    resp = l.client.get("/api/providers/" + str(provider_id) + "/contacts",
                        headers=headers)
    assert resp.status_code == 200
    print resp.content


def get_provider_insurances_list(l, access, approved_providers_list):
    print '### get provider insurances list: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(approved_providers_list)
    resp = l.client.get("/api/providers/" + str(provider_id) + "/insurances",
                        headers=headers)
    assert resp.status_code == 200
    print resp.content


def get_provider_licenses_list(l, access, approved_providers_list):
    print '### get provider licenses list: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    provider_id = random.choice(approved_providers_list)
    resp = l.client.get("/api/providers/" + str(provider_id) + "/licenses",
                        headers=headers)
    assert resp.status_code == 200
    print resp.content


def create_provider(l):
    print '### create provider: ###'
    random_uuid = str(uuid.uuid4())[:8]
    headers = {"Content-Type": "application/json"}
    body = '{"details":{"companyName":"' + random_uuid + '","yearsInBusiness":"11","website":"","numberOfEmployees":"1-10","numberOfServiceVehicles":"1-10","taxId":"11","generalServiceAreas":"","companyOverview":"","servicesToProvideOverview":"","contactFirstName":"' + random_uuid + '","contactLastName":"' + random_uuid + '","contactPhoneNumber":"+380983989487","contactEmail":"365prontotest+' + random_uuid + '@gmail.com","licenses":[],"memberOfNabcep":false,"memberOfUnion":false},"address":{"placeId":"ChIJRWbgYBFawokRT4RP9qECNCI","streetAddress1":"11 Broadway, New York, NY, USA","streetAddress2":"","city":"New York","state":"NY","zip":"10004"},"adminUser":{"firstName":"' + random_uuid + '","lastName":"' + random_uuid + '","email":"365prontotest+' + random_uuid + '@gmail.com"}}'
    resp = l.client.post("/api/providers", headers=headers, data=body)
    assert resp.status_code == 200
    # print resp.content
    return random_uuid


def create_insurance(l, access, list_of_approved_providers):
    print '### create insurance: ###'
    random_uuid = str(uuid.uuid4())[:8]
    provider_id = random.choice(list_of_approved_providers)
    office_id = get_provider_main_office_id_by_provider_id_from_db(provider_id)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    body = '{"aggregateLimit": 1000, "attachments": [{"attachmentType": "URL","name": "https://translate.google.ru","value": "https://translate.google.ru"}],"expirationDate": "2020-10-10","insuranceType": "AUTO","policyNumber": "322wd3434f24","startDate": "2001-10-10"}'
    # print body
    resp = l.client.post("/api/providers/" + str(provider_id) + "/offices/" + str(office_id) + "/insurances",
                         headers=headers, data=body)
    print resp.url
    print resp.content
    assert resp.status_code == 200
    return {"officeId": office_id, "providerId": provider_id}


def get_insurance(l, access, createInsuranceDict):
    print '### get insurance: ###'
    provider_id = createInsuranceDict["providerId"]
    office_id = createInsuranceDict["officeId"]
    insurance_id = get_provider_insurance_id_by_provider_office_id_from_db(office_id)
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    body = '{"aggregateLimit": 1000, "attachments": [{"attachmentType": "URL","name": "https://translate.google.ru","value": "https://translate.google.ru"}],"expirationDate": "2020-10-10","insuranceType": "AUTO","policyNumber": "322wd3434f24","startDate": "2001-10-10"}'
    # print body
    resp = l.client.get(
        "/api/providers/" + str(provider_id) + "/offices/" + str(office_id) + "/insurances/" + str(insurance_id),
        headers=headers, data=body)
    print resp.url
    print resp.content
    assert resp.status_code == 200
    return {"providerId": provider_id, "officeId": office_id, "insuranceId": insurance_id}


def update_insurance(l, access, dict):
    print '### update insurance: ###'
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    body = '{"aggregateLimit": 1000, "attachments": [{"attachmentType": "URL","name": "https://translate.google.ru","value": "https://translate.google.ru"}],"expirationDate": "2020-10-10","insuranceType": "AUTO","policyNumber": "322wd3434f24","startDate": "2001-10-10"}'
    # print body
    resp = l.client.put(
        "/api/providers/" + str(dict["providerId"]) + "/offices/" + str(dict["officeId"]) + "/insurances/" + str(
            dict["insuranceId"]), headers=headers, data=body)
    print resp.url
    print resp.content
    assert resp.status_code == 200


def upload_insurance_attachment(l, access, dict):
    print '### upload insurance attachment: ###'
    files = {'file': open('data.jpeg', 'rb')}
    headers = {"Authorization": "Bearer " + access}
    resp = l.client.post("/api/providers/" + str(dict["providerId"]) + "/offices/" + str(
        dict["officeId"]) + "/insurances/attachments/documents", headers=headers, files=files)
    print resp.url
    print resp.content
    assert resp.status_code == 200
    return resp.content


def get_uploaded_insurance_attachment_document(l, access, dict, id):
    print '### get uploaded insurance attachment document: ###'
    provider_id = dict["providerId"]
    office_id = dict["officeId"]
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    # print body
    resp = l.client.get(
        "/api/providers/" + str(provider_id) + "/offices/" + str(
            office_id) + "/insurances/attachments/documents/" + str(id),
        headers=headers)
    print resp.url
    # print resp.content
    assert resp.status_code == 200


def delete_uploaded_insurance_attachment_document(l, access, dict, id):
    print '### delete uploaded insurance attachment document: ###'
    provider_id = dict["providerId"]
    office_id = dict["officeId"]
    headers = {"Content-Type": "application/json", "Authorization": "Bearer " + access}
    resp = l.client.delete(
        "/api/providers/" + str(provider_id) + "/offices/" + str(
            office_id) + "/insurances/attachments/documents/" + str(id),
        headers=headers)
    print resp.url
    print resp.content
    print resp.status_code
    assert resp.status_code == 204


# for debugging
if __name__ == '__main__':
    print(get_provider_insurance_id_by_provider_office_id_from_db(55))


class ProntoWebUserBehavior(TaskSequence):
    prontoAccessToken = None
    prontoRefreshToken = None
    pronto_user_email = None
    customer_uuid = None
    provider_uuid = None

    @seq_task(1)
    @task(1)
    def pronto_login(self):
        resp_dict = pronto_login(self, get_list_approved_active_pronto_emails_from_db())
        self.prontoAccessToken = resp_dict["accessToken"]
        self.prontoRefreshToken = resp_dict["refreshToken"]

    @seq_task(2)
    @task(1)
    def pronto_re_login(self):
        pronto_re_login(self, access=self.prontoAccessToken, refresh=self.prontoRefreshToken)

    @seq_task(3)
    @task(1)
    def create_pronto_user(self):
        self.pronto_user_email = create_pronto_user(self, access=self.prontoAccessToken)

    @seq_task(4)
    @task(1)
    def pronto_finish_registration(self):
        finish_user_registration(self, get_registration_token_for_all_users_email_from_db(self.pronto_user_email))

    @seq_task(5)
    @task(1)
    def get_users_list(self):
        get_users_list(self, access=self.prontoAccessToken)

    @seq_task(6)
    @task(1)
    def get_customers_list(self):
        get_customers_list(self, access=self.prontoAccessToken)

    @seq_task(7)
    @task(1)
    def get_providers_list(self):
        get_providers_list(self, access=self.prontoAccessToken)

    @seq_task(8)
    @task(1)
    def get_customer_onboarding_info(self):
        get_customer_onboarding_info(self, get_approved_customers_ids_from_db(),
                                     access=self.prontoAccessToken)

    @seq_task(9)
    @task(1)
    def create_customer(self):
        self.customer_uuid = create_customer()
        print self.customer_uuid

    @seq_task(10)
    @task(1)
    def approve_specified_customer_by_pronto(self):
        approve_specified_customer_by_pronto(self, self.customer_uuid, access=self.prontoAccessToken)

    @seq_task(11)
    @task(1)
    def create_provider(self):
        self.provider_uuid = create_provider()
        print self.provider_uuid

    @seq_task(12)
    @task(1)
    def approve_specified_provider_by_pronto(self):
        approve_specified_provider_by_pronto(self, self.provider_uuid, access=self.prontoAccessToken)

    @seq_task(13)
    @task(1)
    def get_provider_offices_list(self):
        get_provider_offices_list(self, access=self.prontoAccessToken,
                                  list_approved_providers_ids=get_approved_providers_ids_from_db())

    @seq_task(14)
    @task(1)
    def get_states_list(self):
        get_states_list(self, access=self.prontoAccessToken)

    @seq_task(15)
    @task(1)
    def get_provider_onboarding_info(self):
        get_provider_onboarding_info(self, list_of_providers_ids=get_approved_providers_ids_from_db(),
                                     access=self.prontoAccessToken)

    @seq_task(16)
    @task(1)
    def get_provider_overview(self):
        get_provider_overview(self, access=self.prontoAccessToken,
                              approved_providers_list=get_approved_providers_ids_from_db())

    @seq_task(17)
    @task(1)
    def pronto_logout(self):
        pronto_logout(self, access=self.prontoAccessToken)


class CustomerWebUserBehavior(TaskSequence):
    customer_uuid = None
    prontoAccessToken = None
    prontoRefreshToken = None
    customerAccessToken = None
    customerRefreshToken = None

    def on_start(self):
        resp_dict = pronto_login(self, get_list_approved_active_pronto_emails_from_db())
        self.prontoAccessToken = resp_dict["accessToken"]

    @seq_task(1)
    @task(1)
    def customer_login(self):
        resp_dict = customer_login(self, get_list_approved_customers_emails_from_db())
        self.customerAccessToken = resp_dict["accessToken"]
        self.customerRefreshToken = resp_dict["refreshToken"]

    @seq_task(2)
    @task(1)
    def customer_re_login(self):
        user_re_login(self, access=self.customerAccessToken, refresh=self.customerRefreshToken)

    @seq_task(3)
    @task(1)
    def customer_onboarding(self):
        self.customer_uuid = customer_onboard(self)

    @seq_task(4)
    @task(1)
    def approve_specified_customer_by_pronto(self):
        approve_specified_customer_by_pronto(self, self.customer_uuid, access=self.prontoAccessToken)

    @seq_task(5)
    @task(1)
    def customer_finish_registration(self):
        customer_email = '365prontotest+' + str(self.customer_uuid) + '@gmail.com'
        finish_user_registration(self, get_registration_token_for_all_users_email_from_db(customer_email))

    @seq_task(6)
    @task(1)
    def get_customer_onboarding_info(self):
        get_customer_onboarding_info(self, get_approved_customers_ids_from_db(),
                                     access=self.customerAccessToken)

    @seq_task(7)
    @task(1)
    def get_party_details_for_customers(self):
        get_users_party_details(self, get_approved_customers_ids_from_db(),
                                access_token=self.customerAccessToken)

    @seq_task(8)
    @task(1)
    def customer_logout(self):
        user_logout(self, access_token=self.customerAccessToken)


class ProviderWebUserBehavior(TaskSequence):
    provider_uuid = None
    prontoAccessToken = None
    providerAccessToken = None
    providerRefreshToken = None
    providerId = None
    createOfficeDict = None
    updatedOfficeContactUrl = None
    providerInsuranceDict = None
    getInsuranceDict = None
    insuranceAttachmentDocumentId = None

    def on_start(self):
        resp_dict = pronto_login(self, get_list_approved_active_pronto_emails_from_db())
        self.prontoAccessToken = resp_dict["accessToken"]

    @seq_task(1)
    @task(1)
    def provider_login(self):
        resp_dict = provider_login(self, get_list_approved_providers_emails_from_db())
        self.providerAccessToken = resp_dict["accessToken"]
        self.providerRefreshToken = resp_dict["refreshToken"]

    # @seq_task(2)
    # @task(1)
    # def provider_onboarding(self):
    #     self.provider_uuid = create_provider()
    #     print self.provider_uuid
    #
    # @seq_task(3)
    # @task(1)
    # def approve_specified_provider_by_pronto(self):
    #     approve_specified_provider_by_pronto(self, self.provider_uuid, access=self.prontoAccessToken)
    #
    # @seq_task(4)
    # @task(1)
    # def provider_finish_registration(self):
    #     provider_email = '365prontotest+' + str(self.provider_uuid) + '@gmail.com'
    #     finish_user_registration(self, get_registration_token_for_all_users_email_from_db(provider_email))
    #
    # @seq_task(5)
    # @task(1)
    # def get_states_list(self):
    #     get_states_list(self, self.prontoAccessToken)
    #
    # @seq_task(6)
    # @task(1)
    # def get_provider_overview(self):
    #     get_provider_overview(self, access=self.prontoAccessToken,
    #                           approved_providers_list=get_approved_providers_ids_from_db())
    #
    # @seq_task(7)
    # @task(1)
    # def get_provider_onboarding_info(self):
    #     get_provider_onboarding_info(self, list_of_providers_ids=get_approved_providers_ids_from_db(),
    #                                  access=self.prontoAccessToken)
    #
    # @seq_task(8)
    # @task(1)
    # def get_party_details_for_providers(self):
    #     get_users_party_details(self, get_approved_providers_ids_from_db(),
    #                             access_token=self.providerAccessToken)
    #
    # @seq_task(9)
    # @task(1)
    # def provider_re_login(self):
    #     user_re_login(self, access=self.providerAccessToken, refresh=self.providerRefreshToken)
    #
    # @seq_task(10)
    # @task(1)
    # def get_provider_offices_list(self):
    #     get_provider_offices_list(self, access=self.providerAccessToken,
    #                               list_approved_providers_ids=get_approved_providers_ids_from_db())
    #
    # @seq_task(11)
    # @task(1)
    # def create_office_contact(self):
    #     self.createOfficeDict = create_office_contact(self,
    #                                                   access=self.prontoAccessToken,
    #                                                   list_of_approved_providers=get_approved_providers_ids_from_db())
    #
    # @seq_task(12)
    # @task(1)
    # def update_office_contact(self):
    #     self.updatedOfficeContactUrl = update_office_contact(self, access=self.prontoAccessToken,
    #                                                          dict_with_provider_id_and_office_id=self.createOfficeDict)
    #
    # @seq_task(13)
    # @task(1)
    # def delete_office_contact(self):
    #     delete_office_contact(self, access=self.prontoAccessToken, url_to_delete=self.updatedOfficeContactUrl)
    #
    # @seq_task(14)
    # @task(1)
    # def create_provider_office(self):
    #     create_provider_office(self, access=self.prontoAccessToken,
    #                            list_of_approved_providers=get_approved_providers_ids_from_db())

    # @seq_task(15)
    # @task(1)
    # def update_provider_overview(self):
    #     update_provider_overview(self, access=self.providerAccessToken,
    #                              list_of_approved_providers=get_approved_providers_ids_from_db())
    #
    # @seq_task(16)
    # @task(1)
    # def get_provider_contact_list(self):
    #     get_provider_contact_list(self, access=self.prontoAccessToken,
    #                               approved_providers_list=get_approved_providers_ids_from_db())
    #
    # @seq_task(17)
    # @task(1)
    # def get_provider_insurances_list(self):
    #     get_provider_insurances_list(self, access=self.prontoAccessToken,
    #                                  approved_providers_list=get_approved_providers_ids_from_db())
    #
    # @seq_task(18)
    # @task(1)
    # def get_provider_licenses_list(self):
    #     get_provider_licenses_list(self, access=self.prontoAccessToken,
    #                                approved_providers_list=get_approved_providers_ids_from_db())

    @seq_task(19)
    @task(1)
    def create_insurance(self):
        self.providerInsuranceDict = create_insurance(self, access=self.prontoAccessToken,
                                                      list_of_approved_providers=get_approved_providers_ids_from_db())

    @seq_task(20)
    @task(1)
    def get_insurance(self):
        self.getInsuranceDict = get_insurance(self, access=self.prontoAccessToken,
                                              createInsuranceDict=self.providerInsuranceDict)

    @seq_task(21)
    @task(1)
    def update_insurance(self):
        update_insurance(self, access=self.prontoAccessToken, dict=self.getInsuranceDict)

    @seq_task(22)
    @task(1)
    def upload_file(self):
        self.insuranceAttachmentDocumentId = upload_insurance_attachment(self, access=self.prontoAccessToken,
                                                                         dict=self.getInsuranceDict)

    @seq_task(23)
    @task(1)
    def get_uploaded_insurance_attachment_document(self):
        get_uploaded_insurance_attachment_document(self, access=self.prontoAccessToken, dict=self.getInsuranceDict,
                                                   id=self.insuranceAttachmentDocumentId)

    @seq_task(24)
    @task(1)
    def delete_uploaded_insurance_attachment_document(self):
        delete_uploaded_insurance_attachment_document(self, access=self.prontoAccessToken, dict=self.getInsuranceDict,
                                                      id=self.insuranceAttachmentDocumentId)


class ProntoWebUser(HttpLocust):
    weight = 1
    host = 'test-2.365pronto.net'
    task_set = ProntoWebUserBehavior
    min_wait = 1000
    max_wait = 5000


class CustomerWebUser(HttpLocust):
    weight = 1
    host = 'test-2.365pronto.net'
    task_set = CustomerWebUserBehavior
    min_wait = 1000
    max_wait = 5000


class ProviderWebUser(HttpLocust):
    weight = 2
    host = 'test-2.365pronto.net'
    task_set = ProviderWebUserBehavior
    min_wait = 1000
    max_wait = 5000
