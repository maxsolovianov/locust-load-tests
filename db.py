import psycopg2


def get_customer_id_by_customer_name(customer_name):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT customer_onboarding_id FROM customer WHERE company_name=" + "'" + customer_name + "'")
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_provider_id_by_provider_name(provider_name):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT provider_onboarding_id FROM provider WHERE company_name=" + "'" + provider_name + "'")
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_last_created_contact_id_by_office_id(office_id):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT id FROM provider_office_contact WHERE provider_office_id =" + str(office_id) + "AND contact_type = 'ALTERNATIVE' ORDER BY created_at DESC")
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_pronto_id_by_pronto_name(customer_name):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT customer_onboarding_id FROM customer WHERE company_name=" + "'" + customer_name + "'")
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_provider_main_office_id_by_provider_id_from_db(provider_id):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT provider_main_office_id FROM provider WHERE id =" + str(provider_id))
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_provider_insurance_id_by_provider_office_id_from_db(provider_office_id):
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT provider_insurance_id FROM provider_office_provider_insurance WHERE provider_office_id =" + str(provider_office_id))
    records = cursor.fetchall()
    id = str(records[0])
    return id[1:-2]


def get_approved_customers_ids_from_db():
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT customer_onboarding_id FROM customer WHERE customer.status='APPROVED'")
    records = cursor.fetchall()
    # convert list of tuples to list
    return [i[0] for i in records]


def get_approved_providers_ids_from_db():
    conn = psycopg2.connect(dbname='all-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net', port=5434)
    cursor = conn.cursor()
    cursor.execute("SELECT provider_onboarding_id FROM provider WHERE provider.status='APPROVED'")
    records = cursor.fetchall()
    # convert list of tuples to list
    return [i[0] for i in records]


def get_list_approved_customers_emails_from_db():
    conn = psycopg2.connect(dbname='user-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net')
    cursor = conn.cursor()
    cursor.execute(
        'SELECT email FROM "public"."user" WHERE "public"."user".active = true AND "public"."user".association = \'CUSTOMER\'')
    records = cursor.fetchall()
    # convert list of tuples to list
    return [i[0] for i in records]


def get_list_approved_providers_emails_from_db():
    conn = psycopg2.connect(dbname='user-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net')
    cursor = conn.cursor()
    cursor.execute(
        'SELECT email FROM "public"."user" WHERE "public"."user".active = true AND "public"."user".association = \'PROVIDER\'')
    records = cursor.fetchall()
    # convert list of tuples to list
    return [i[0] for i in records]


def get_list_approved_active_pronto_emails_from_db():
    conn = psycopg2.connect(dbname='user-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net')
    cursor = conn.cursor()
    cursor.execute(
        'SELECT email FROM "public"."user" WHERE "public"."user".active = true AND "public"."user".association = \'PRONTO\'')
    records = cursor.fetchall()
    # convert list of tuples to list
    return [i[0] for i in records]


def get_registration_token_for_all_users_email_from_db(email):
    import psycopg2
    conn = psycopg2.connect(dbname='user-service-db', user='pronto',
                            password='superpasswd', host='test-2.365pronto.net')
    cursor = conn.cursor()
    cursor.execute(
        'SELECT token_value FROM "public"."user_registration_token" INNER JOIN "public"."user" ON user_registration_token.user_id = "user"."id" WHERE "user".email = ' + "'" + email + "'" + '')
    records = cursor.fetchone()
    reg_token = str(records)
    print 'registration token for email ' + email + ' is: ' + reg_token[2:-3]
    return reg_token[2:-3]
