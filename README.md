**Locust** *load testing tool*

---

## How to use
1. If you don't yet have Python 3, [download and install first](https://www.python.org/downloads/). If you prefer to install from the command line, see [Python 3 Installation & Setup Guide](https://realpython.com/installing-python/).
2. Install [virtualenv](https://virtualenv.pypa.io/en/stable/) globally to create isolated Python environment
3. Download an existing Git repository to your local computer with: **git clone**
4. Into project directory create virtualenv dir: **virtualenv -p python3 venv**
5. Activate your virtualenv: **. venv/bin/activate** or **source venv/bin/activate**
6. Install project dependencies: **pip install -r requirements.txt**
7. Run program: **locust --host=https://app.nur.stage.systools.pro**
8. Go to *http://localhost:8089/*